using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.XR.MagicLeap;
using static UnityEngine.LightAnchor;

public class BoundingBox
{
    public Vector3 position;
    public Quaternion orientation;
    public Vector3 scale;

    public static BoundingBox fromCoordinates(Vector3 origin, Vector3 rightPosition, Vector3 upPosition)
    {
        Vector3 rightDirection = rightPosition - origin;
        Vector3 upDirection = upPosition - origin;
        Vector3 forwardDirection = Vector3.Cross(upDirection, rightDirection);

        BoundingBox b = new BoundingBox();
        b.position = origin;
        b.orientation = Quaternion.LookRotation(forwardDirection, upDirection);
        float width = Vector3.Distance(origin, rightPosition);
        float height = Vector3.Distance(origin, upPosition);
        b.scale = new Vector3(width, height, 0.02f);
        return b;
    }

    public override string ToString()
    {
        return position + " | " + orientation.eulerAngles + " | " + scale;
    }
}

public class markerTrackingTest : MonoBehaviour
{
    [SerializeField]
    private GameObject markerObject;

    [SerializeField]
    private GameObject container;

    private MagicLeapInputs mlInputs;
    private Dictionary<string, GameObject> markers = new();
    private LineRenderer line;

    public BoundingBox getBoudingBox()
    {
        if (!markers.ContainsKey("3")
            && !markers.ContainsKey("4")
            && !markers.ContainsKey("2"))
        {
            return null;
        }

        BoundingBox b = BoundingBox.fromCoordinates(markers["4"].transform.position, markers["3"].transform.position, markers["1"].transform.position);

        container.transform.SetPositionAndRotation(b.position, b.orientation);
        container.transform.localScale = b.scale;
        Vector3 translation = b.scale / 2;
        translation.x = -translation.x;
        container.transform.Translate(translation);

        return b;
    }

    // Start is called before the first frame update
    void Start()
    {
        MyLogger.write("\n\n" + DateTime.Now);
        try
        {
            mlInputs = new MagicLeapInputs();
            mlInputs.Enable();

            bool enableMarkerScanning = true;
            float qrCodeMarkerSize = 0.01f;
            float arucoMarkerSize = 0.01f;
            MLMarkerTracker.MarkerType type = MLMarkerTracker.MarkerType.Aruco_April;
            MLMarkerTracker.ArucoDictionaryName arucoDict = MLMarkerTracker.ArucoDictionaryName.DICT_5X5_100;
            MLMarkerTracker.Profile trakerProfile = MLMarkerTracker.Profile.Default;
            MLMarkerTracker.TrackerSettings settings = MLMarkerTracker.TrackerSettings.Create(enableMarkerScanning, type, qrCodeMarkerSize, arucoDict, arucoMarkerSize, trakerProfile, default);
            MLMarkerTracker.SetSettingsAsync(settings).GetAwaiter().GetResult();

            MagicLeapInputs.ControllerActions controllerActions = new MagicLeapInputs.ControllerActions(mlInputs);
            controllerActions.Trigger.performed += HandleOnTrigger;
        }
        catch (Exception e)
        {
            Debug.Log($"CombinedTrackingExample.EnableMarkerTrackerExample() => error: {e.Message}");
        }
    }

    private void HandleOnTrigger(InputAction.CallbackContext obj)
    {
        MyLogger.write("Camera Position: " + Camera.main.transform.position + " Rtoation: " + Camera.main.transform.rotation.eulerAngles
         + "\nBounding Box: " + getBoudingBox()
         + "\n");
    }

    // Update is called once per frame
    void Update()
    {
        if (
            !markers.ContainsKey("1") 
            || !markers.ContainsKey("2")
            || !markers.ContainsKey("3")
            || !markers.ContainsKey("4"))
        {
            return;
        }

        if (!line) {
            GameObject l = new GameObject("Border");
            l.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            line = l.AddComponent<LineRenderer>();
            line.positionCount = 4;
            line.alignment = LineAlignment.TransformZ;
            line.loop = true;
        }

        line.SetPositions(new Vector3[4]{
            markers["1"].transform.position,
            markers["2"].transform.position,
            markers["3"].transform.position,
            markers["4"].transform.position,
        });
    }

    private void OnEnable()
    {
        MLMarkerTracker.OnMLMarkerTrackerResultsFound += OnTrackerResultsFound;
    }

    private void OnDisable()
    {
        MLMarkerTracker.OnMLMarkerTrackerResultsFound -= OnTrackerResultsFound;
    }

    private void OnTrackerResultsFound(MLMarkerTracker.MarkerData data)
    {
        if (data.Type == MLMarkerTracker.MarkerType.Aruco_April)
        {
            // Aruco markers contains additional information like which dictionary they belong to.
            string arucoId = data.ArucoData.Id.ToString();
            GameObject o;

            if (!markers.ContainsKey(arucoId))
            {
                MyLogger.write(arucoId + "Position:" + data.Pose.position + " Rotation:" + data.Pose.rotation.eulerAngles);
                o = Instantiate(markerObject);
                o.name = "Aruco" + arucoId;
                markers.Add(arucoId, o);
            }
            else
            {
                o = markers[arucoId];
            }

            o.transform.position = data.Pose.position;
            o.transform.rotation = data.Pose.rotation;
        }
    }
}
