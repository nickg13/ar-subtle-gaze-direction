using System;
using UnityEngine;

public class CueTransformAround : MonoBehaviour
{
    //[SerializeField]
    public int interval = 6000;

    [SerializeField]
    private float radius = 0.025f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("TransformAround", 0, 0.01f);
    }

    private int m = 0;
    // Modulates the hue between red and blue
    void TransformAround()
    {
        m = (m + 1) % interval;
        double deg = ((double)m / (double)interval) * 360;
        float x = radius * (float)Math.Cos(deg);
        float y = radius * (float)Math.Sin(deg);
        this.transform.position = origin;
        this.transform.Translate(x, y, 0);
    }


    private Vector3 origin;
    private void OnEnable()
    {
        Debug.Log("Enabled " + this.transform.position);
        origin = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
