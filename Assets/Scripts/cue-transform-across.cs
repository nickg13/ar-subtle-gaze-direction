using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueTransformAcrossModulation : MonoBehaviour
{
    //[SerializeField]
    public int interval = 20;

    [SerializeField]
    private float maxDistance = 0.025f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("TransformAcross", 0, 0.01f);
    }

    private int k = 0;
    private int l = 1;
    // Modulates the hue between red and blue
    void TransformAcross()
    {
        k = (k + l) % interval;
        if (k == 0)
        {
            l *= -1;
        }
        float dist = ((float)l / (float)interval) * maxDistance;
        this.transform.Translate(dist, 0, 0);
    }

    private Vector3 origin;
    private void OnEnable()
    {
        Debug.Log("Enabled " + this.transform.position);
        origin = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
