using UnityEngine;

public class CueLightModulation : MonoBehaviour
{
    //[SerializeField]
    public int interval = 100;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ModulateLuminance", 0, 0.01f);
    }

    private int j = 0;
    // Modulates the hue between red and blue
    void ModulateLuminance()
    {
        j = (j + 1) % interval;
        float v = j < interval / 2 ? 1 : 0;
        this.GetComponent<MeshRenderer>().material.color = Color.HSVToRGB(0, 0, v);
    }

    private Vector3 origin;
    private void OnEnable()
    {
        Debug.Log("Enabled " + this.transform.position);
        origin = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
