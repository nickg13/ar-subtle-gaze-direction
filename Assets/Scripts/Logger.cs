// %BANNER_BEGIN%
// ---------------------------------------------------------------------
// %COPYRIGHT_BEGIN%
// Copyright (c) (2021-2022) Magic Leap, Inc. All Rights Reserved.
// Use of this file is governed by the Software License Agreement, located here: https://www.magicleap.com/software-license-agreement-ml2
// Terms and conditions applicable to third-party materials accompanying this distribution may also be found in the top-level NOTICE file appearing herein.
// %COPYRIGHT_END%
// ---------------------------------------------------------------------
// %BANNER_END%

using System.IO;
using UnityEngine;

public class MyLogger
{
    StreamWriter sw;

    public static void write(string data)
    {
        StreamWriter sw = new StreamWriter(Application.persistentDataPath + "/data.log", true);
        Debug.Log(data);
        sw.WriteLine(data);
        sw.Close();
    }
}
