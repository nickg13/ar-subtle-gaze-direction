using UnityEngine;

public class CueRedBlueModulation : MonoBehaviour
{
    //[SerializeField]
    public int interval;

    [SerializeField]
    private float maxDistance = 0.025f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ModulateHue", 0, 0.01f);
    }

    private int i = 0;
    // Modulates the hue between red and blue
    void ModulateHue()
    {
        i = (i + 1) % interval;
        float v = i < interval / 2 ? 1 : .666f;
        this.GetComponent<MeshRenderer>().material.color = Color.HSVToRGB(v, 1, 1);
    }

    private Vector3 origin;
    private void OnEnable()
    {
        Debug.Log("Enabled " + this.transform.position);
        origin = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
