using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Cue : MonoBehaviour
{
    [SerializeField]
    markerTrackingTest t;

    [SerializeField]
    GameObject cue;

    [SerializeField]
    List<GameObject> cues;

    private MagicLeapInputs mlInputs;

    // Start is called before the first frame update
    void Start()
    {
        mlInputs = new MagicLeapInputs();
        mlInputs.Enable();
        MagicLeapInputs.ControllerActions controllerActions = new MagicLeapInputs.ControllerActions(mlInputs);
        controllerActions.Bumper.performed += HandleOnBumper;
    }

    private int index = 0;
    private bool isActive = false;
    private void HandleOnBumper(InputAction.CallbackContext obj)
    {
        if (isActive)
        {
            DisableCue();
        }
        else
        {
            EnableCue();
        }
        isActive = !isActive;
    }

    private void DisableCue()
    {
        cue = cues[index];
        cue.SetActive(false);
        index = (index + 1) % cues.Count;
    }

    private void EnableCue()
    {
        cue = cues[index];
        BoundingBox b = t.getBoudingBox();
        cue.transform.position = b.position;
        cue.transform.rotation = b.orientation;

        MyLogger.write("Cue0: " + cue.transform.position);

        float xDelta = -Random.Range(0f, b.scale.x);
        float yDelta = Random.Range(0f, b.scale.y);

        cue.transform.Translate(xDelta, yDelta, 0);
        MyLogger.write("Cue1: " + cue.transform.position);
        cue.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
